import './App.css';
import ContactList from './components/ContactList/ContactList';
import Header from './components/Header/Header';

function App() {
  return (
    <div>
      <Header />
      <ContactList />
    </div>
  )
}

export default App;
