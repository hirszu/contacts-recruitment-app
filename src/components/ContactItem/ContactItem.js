import React, { useState } from 'react';

import './ContactItem.css'

const ContactItem = ({id, first_name, last_name, email, gender, avatar, handleCheck}) => {
  
  const [checked, setChecked] = useState(false);

  const handleClick = (e) => {
    const newState = !checked;
    handleCheck(id, newState);
    setChecked(newState)
  }
  
  return (
    <div className='contact-item' onClick={handleClick}>
      <div className='avatar'>
        {avatar && <img src={avatar} alt='avatar' />}
      </div>
      <div className='user'>
        <div className='name'>
          {first_name} {last_name}
        </div>
        <div className='email'>
          {email}
        </div>
      </div>
      <input type='checkbox' checked={checked} onChange={handleClick}/>
    </div>
  );
};

export default ContactItem;