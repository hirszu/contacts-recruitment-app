import React, { useState } from 'react';

import './SearchBar.css'

const SearchBar = ({handleSearch}) => {
  const [term, setTerm] = useState('')

  const handleChange = (e) => {
    setTerm(e.target.value)
  }

  const handleKey = (key) => {
    if (key.code === 'Enter') {
      handleSearch(term);
    }
  }

  return (
  <div>
    <input className='search' type='text' placeholder='Search term' value={term} onChange={handleChange} onKeyPress={handleKey}/>

  </div>
  );
};

export default SearchBar;