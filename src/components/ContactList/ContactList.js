import React, { useEffect, useState } from 'react';
import ContactItem from '../ContactItem/ContactItem';
import SearchBar from '../SearchBar/SearchBar';

import "./ContactList.css"

const ContactList = () => {
  const API = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

  const [contacts, setContacts] = useState([])
  const [selected, setSelected] = useState([])
  const [searched, setSearched] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(API);
      const jsonData = await response.json();
      const sorted = jsonData.sort((a, b) => (a.last_name > b.last_name ? 1 : -1))
      setContacts(sorted)
      setSearched(sorted)
    }

    fetchData()
    
  }, []);

  const handleCheck = (id, state) => {
    if (state) {
      setSelected([...selected, id])
    }
    else {
      const indexToRemove = selected.findIndex(el => el === id);
      const newArr = selected.filter((el, index) => index !== indexToRemove)
      setSelected(newArr)
    }

  }

  useEffect(() => {
    console.log(selected)
  }, [selected])



  const handleSearch = (term) => {
    const filtered = contacts.filter(user => {
      if (user.last_name.toLowerCase().includes(term.toLowerCase())) return true
      if (user.first_name.toLowerCase().includes(term.toLowerCase())) return true
      return false
    })

    setSearched(filtered)
  }

  return (
    <div>
      <SearchBar handleSearch={handleSearch}/>
      {searched && searched.map(contact => {
        return (
          <ContactItem  
            {...contact}
            handleCheck={handleCheck} 
            key={contact.id}
          />
        )
        })}
    </div>
  );
};

export default ContactList;